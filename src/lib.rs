//! AsnLength
// This structure wraps the legnth of Asn members
// It will encode the length in a byte array suitable to be passed
// over the wire. This implementation is limited to elements having
// A maximum length of 16 extabytes. The theoritical limit is
// 1.58E29 Gigabytes so in the future this might need to be expanded
#[derive(Clone,Copy)]
struct AsnLength {
    value: usize
}

impl AsnLength {

    fn from_bytes(raw: Box<[u8]>) -> AsnLength {
        AsnLength { value: 0 }
    }
    
    // This signals that the length can be stored in a single 7 bit
    // value
    fn single_octet(self) -> bool {
        self.value < 128
    }
    
    fn length(self) -> usize {
        // Special case we encode the length in a single octect
        if self.single_octet() {
            return 1
        }
        let s = std::mem::size_of::<usize>();
        
        // The number of Bytes we actully need to encode the number
        // Plus the flag byte
        let rvalue = s*8 - self.value.leading_zeros() as usize;
        let rvalue = match rvalue {
            x if x%8 == 0 => x/8,
            x => x/8+1
        };
        rvalue + 1
    }

    fn bytes(self) -> Box<[u8]> {
        unsafe {
            let length = self.length();
            let mut rvalue = Vec::with_capacity(length);
            rvalue.set_len(length);
            if self.single_octet() {
                let b = std::mem::transmute::<_, *mut u8>(&self.value);
                std::ptr::copy_nonoverlapping(b,rvalue.as_mut_ptr(),1);            
            } else {
                let lmod = length - 1 + 128;                
                let b = std::mem::transmute::<_, *mut u8>(&lmod);
                std::ptr::copy_nonoverlapping(b,rvalue.as_mut_ptr(),1);

                let b = std::mem::transmute::<_, *mut u8>(&self.value);
                std::ptr::copy_nonoverlapping(b,rvalue.as_mut_ptr().offset(1),length-1);
            }
            rvalue.into_boxed_slice()
        }
    }
}

pub trait AsnValue {
    fn identifier(self) -> u8;
    fn length(self) -> usize;
    fn contents(self) -> Vec<Box<AsnValue>>;
    fn bytes(self) -> Box<[u8]>;
}


#[derive(Clone,Copy)]
struct AsnInteger { 
    value: i64
}

type AsnIntegerBacking = i64;

impl AsnValue for AsnInteger { 

    fn identifier(self) -> u8 { 
        0x02
    }
    fn length(self) -> usize {
        //There are three parts to the length. 
        // Tag
        // Length
        // Data
        1 + (AsnLength { value:std::mem::size_of::<AsnIntegerBacking>() }).length() + std::mem::size_of::<AsnIntegerBacking>()
    }
    fn contents(self) -> Vec<Box<AsnValue>> { 
        // This type has no contents 
        Vec::new()
    }
    fn bytes(self) -> Box<[u8]> { 
        let size = self.length();
        unsafe { 
            let mut rvalue = Vec::with_capacity(size);
            rvalue.set_len(size);

            let length = AsnLength { value: std::mem::size_of::<AsnIntegerBacking>() };            
            let length_bytes = length.bytes();

            // Transmute the pointers for unsafe copying
            let l = std::mem::transmute::<_, *mut u8>(length_bytes.as_ptr());
            let b = std::mem::transmute::<_, *mut u8>(&self.value);

            std::ptr::copy_nonoverlapping(&(self.identifier()), rvalue.as_mut_ptr(),1);            
            std::ptr::copy_nonoverlapping(l, rvalue.as_mut_ptr().offset(1),length.length());
            std::ptr::copy_nonoverlapping(b, rvalue.as_mut_ptr().offset(length.length() as isize+1),std::mem::size_of::<AsnIntegerBacking>());
            rvalue.into_boxed_slice()
        }
    }

}

#[test]
fn trial_length() {
    let t0 = AsnLength {value: std::mem::size_of::<AsnIntegerBacking>() };
    assert_eq!(t0.length(),1);
    assert_eq!(&(*t0.bytes()),&[8]);

    let t1 = AsnLength { value: 1 };
    assert_eq!(t1.length(),1);
    assert_eq!(&(*t1.bytes()),&[1]);

    // Test out 128 we should have a flagged length
    // Octet saying we have one octect of actual length
    // and that octect should contain the number
    // 128 so the toal size should be two 
    let t2 = AsnLength { value: 128 };
    assert_eq!(t2.length(),2);

    // Make sure we encoded it correctly 
    assert_eq!(&(*t2.bytes()),&[129,128]);

    let t3 = AsnLength { value: 257 };
    assert_eq!(t3.length(),3);

    let t4 = AsnLength { value: std::u64::MAX as usize };
    assert_eq!(t4.length(),9);
    assert_eq!(&(*t4.bytes()),&[136,255,255,255,255,255,255,255,255]);

    let t5 = AsnLength { value: 127 } ;
    assert_eq!(t5.length(),1);
    assert_eq!(&(*t5.bytes()),&[127]);
}

#[test]
fn how_big_is_a_int() { 
    assert_eq!(std::mem::size_of::<i64>(),8);
}

#[test]
fn encode_zero() {
    let t1 = AsnInteger { value: 0 }; 
    assert!( t1.bytes().len() == 10);
    assert_eq!(&(*t1.bytes()), &[2,8,0,0,0,0,0,0,0,0]);
}

#[test]
fn encode_one() { 
    let t1 = AsnInteger { value:1 }; 
    assert_eq!(&(*t1.bytes()), &[2,8,1,0,0,0,0,0,0,0]);
}
